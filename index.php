
<!DOCTYPE html>
<html ng-app="Merck">

<head>
	<base href="/wordpress/">
	<title>Merck</title>
	<meta charset="utf-8">
	<meta name="theme-color" content="#115ba6" />
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">               



	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/stylesheets/miligram.min.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/stylesheets/ionic.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/stylesheets/style.css?rev=1">
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700" rel="stylesheet">

	<?php wp_head(); ?>
</head>

<body ng-controller="MainController">
	<?php 
	

		if( isset($_SESSION['merck']) ){
			$loged_user  =$_SESSION['merck'];
			unset( $loged_user->user_pass );	
			$allowed = get_user_meta($loged_user->ID, 'allowed', true);		
			$loged_user->data->allowed = $allowed;
			echo '<script> var user = '.json_encode(  $loged_user->data ).';</script>';
		}

	?>
	<ion-nav-view></ion-nav-view>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.7/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/select/1.0.0/js/dataTables.select.min.js"></script>

	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/Merck.js?s"></script>
	<?php wp_footer(); ?>
</body>
</html>
