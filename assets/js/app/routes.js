angular
	.module('Merck', [
		'ionic',
		'ngMask',
		'ui.router',
		'ngAnimate',
	]).config(['$stateProvider', '$urlRouterProvider', '$ionicConfigProvider',
	function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

		$ionicConfigProvider.views.transition('fade');

		$stateProvider.
			state('/splash', {
				url: '/splash',
				templateUrl: 'wp-content/themes/merck/templates/splash.html',
				controller: 'MainController',
			}).
			state('/initial', {
				url: '/initial',
				templateUrl: 'wp-content/themes/merck/templates/initial.html',
				controller: 'InitialController',
			}).
			state('auth', {
				url: '/auth',
				templateUrl: 'wp-content/themes/merck/templates/auth.html',
				controller: 'AuthController',
			}).			
			state('/quiz', {
				url: '/quiz',
				templateUrl: 'wp-content/themes/merck/templates/quiz.html',
				controller: 'QuizController',
			}).
			state('/complete', {
				url: '/complete',
				templateUrl: 'wp-content/themes/merck/templates/complete.html',
				controller: 'MainController',
			}).	
			state('/adminauth', {
				url: '/adminauth',
				templateUrl: 'wp-content/themes/merck/templates/adminauth.html',
				controller: 'MainController',
			}).	
			state('/admin-users', {
				url: '/admin-users',
				templateUrl: 'wp-content/themes/merck/templates/admin-users.html',
				controller: 'AdminController',
			}).
			state('/edit/', {
				url: '/edit/:id',
				templateUrl: 'wp-content/themes/merck/templates/edit.html',
				controller: 'EditController',
			});		


			$urlRouterProvider.otherwise('/splash');


	
	}]).run(function($timeout,$window,$ionicPlatform, $q, $state, $rootScope) {

		console.log('app ready')
		    
	});