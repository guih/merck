angular
	.module('Merck')
	.controller('EditController', EditController);

EditController.$inject = [ 
    '$interval',
    '$window',
	'$scope', 
	'$timeout',
    '$state',
    '$ionicSideMenuDelegate',
    '$rootScope',
    'User'
];

function EditController($interval, $window, $scope, $timeout, $state, $ionicSideMenuDelegate, $rootScope, User){
    $scope.user = {};
    $scope.isLoading = true;
    $scope.goTo = function(path){
        $state.go(path, {location: 'replace'})
    }
    User.fetchUser( $state.params.id ).then(function(myUser){
    	$scope.editUser = myUser.data;
    	$scope.isLoading = false;

    });	
};