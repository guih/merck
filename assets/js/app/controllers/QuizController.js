
angular
	.module('Merck')
	.controller('QuizController', QuizController);

QuizController.$inject = [ 
    '$interval',
    '$window',
	'$scope', 
	'$timeout',
    '$state',
    '$ionicSideMenuDelegate',
    '$rootScope',
    'User',
    'QuizService'
    
];

function QuizController($interval, $window, $scope, $timeout, $state, $ionicSideMenuDelegate, $rootScope, User, QuizService){

	$scope.page = 0;

    $scope.loading = true;
    $rootScope.quizAnswer =null;


    console.log('$window',$window)

    QuizService.fetch().then(function(res){
        $rootScope.quiz = res.data;
        $scope.loading = false;
        $scope.display = $rootScope.quiz;
        $rootScope.quiz = $scope.paginate($rootScope.quiz,5, $scope.page);

    });
    $scope.paginate = function (array, page_size, page_number) {
      --page_number; 
      return array.slice(page_number * page_size, (page_number + 1) * page_size);
    }


    $scope.percent = 0;

    $scope.$watch('quiz', function(o, n){
        // console.log(o,n, 'QUIZZZ')

    })

	$scope.display = $rootScope.quiz;
    $scope.pageSize = 5;
    $scope.page = 1;



    
    $scope.resetState = function(index, items){
        if( items.length ==2 ){
            $scope.quiz[index].hasError = false;
        }
        if(  items.length != 2 ){
            if( !$scope.quiz[index].hasError )
                $scope.quiz[index].hasError = null;


            $scope.quiz[index].hasError = false;
        }

        delete $scope.quiz[index].hasError;
    }

    $scope.parseChk = function(data, index){
        console.log(data, index )
        var $opts = $rootScope.quiz[index]._;


        $rootScope.quiz[index].hasError = false;


        if( ! $rootScope.quiz[index]._ )
             $rootScope.quiz[index]._ = [];




        if(   $rootScope.quiz[index]._.indexOf(data.val) == -1 ){

             $rootScope.quiz[index]._.push(data.val);
        console.log('ADD', $rootScope.quiz)

             return;
        }


        if(   $rootScope.quiz[index]._.indexOf(data.val) != -1 )  {
             $rootScope.quiz[index]._ = $rootScope.quiz[index]._.filter(e => e !== data.val);
             if( $rootScope.quiz[index]._.length ==0 ){
                $rootScope.quiz[index]._ = undefined;
             }
        console.log('REMOVE', $rootScope.quiz)

             return;

        }        






    };
    $scope.nextPage = function(){

        console.log($window.user);

        QuizService.setQuiz($rootScope.quiz);

        console.log(QuizService.getQuiz().length, 'GTEE   QUIZZZ')
        
        if( QuizService.getQuiz().length >= 52 ){
            $scope.loading = true;
            
        
            QuizService.saveAnswers({
                data:QuizService.getQuiz(), 
                user: $window.user 
            }).then(function(){
                $state.go('/complete');
                $scope.loading = false;

            }, function(err){

            });
            return;
        }
 
        $rootScope.quiz.forEach(function(val, index){ 
            if(val._ == undefined){
                $rootScope.quiz[index].hasError = true;
            } 
            
        })


        var isErr = $rootScope.quiz.filter(function(v){return v.hasError==true})
        if( isErr.length > 0 ){ return; }
      	$scope.page++;
      	$rootScope.quiz = $scope.paginate($scope.display,5, $scope.page)
      	$scope.percent = Math.round((100 * ((($scope.page-1))*5)) / $scope.display.length);
    }
    $scope.prevPage = function(){
        QuizService.popQuiz($rootScope.quiz);

        if( $scope.page == 1 ) {
            $scope.percent = 0;

            return;
        }

        $scope.page--;


        $rootScope.quiz = $scope.paginate($scope.display,5, $scope.page);
        $scope.percent = Math.round((100 * (($scope.page-1)*5)) / $scope.display.length);
        if( $scope.page == 1 ) $scope.percent = 0;




    }  

  	$scope.$watch('percent', function(newValue, oldValue) {
  		console.log(parseInt(oldValue))
  		if( parseInt(oldValue) > 86 ){
  			$scope.percent = 100;
  		}
  		if( parseInt(oldValue) == 100 ){
  			$scope.percent = 85;
			
		}

	});
};