angular
	.module('Merck')
	.controller('AuthController', AuthController);

AuthController.$inject = [ 
'$interval',
    '$window',
	'$scope', 
	'$timeout',
    '$state',
    '$ionicSideMenuDelegate',
    '$rootScope',
    'User',
];

function AuthController($interval, $window, $scope, $timeout, $state, $ionicSideMenuDelegate, $rootScope, User){
    $scope.user = {};
    console.log('auth')

    $scope.login = function(){
        User.login($scope.user).then(function(res){
            var data = res.data;

            if( data.error ){
                $scope.message = data.error;
            }else{
                User.setUser(JSON.stringify(data));
                
                $state.go('/admin-users', JSON.stringify(data), { reload: false });
            }
        });
    }


    $scope.loginDob = function(){
        $scope.loginLoad = true;
        User.loginDob($scope.user).then(function(res){
            var data = res.data;

            if( data.error ){
                $scope.message = data.error;
            }else{
                User.setUser(JSON.stringify(data));
                $timeout(function() {
                    $scope.loginLoad = false;
                    $window.location.href = document.location.pathname+'/#/quiz'

                }, 2000);
                //$state.go('/admin-users', JSON.stringify(data), { reload: false });
            }
        }, function(e){
            $scope.loginMsg = e.data.message;
            $scope.loginLoad = false;

        });
    }




    $scope.logout = function(){
        User.erase(function(){
            $state.go('/houses');
        });
    }
};