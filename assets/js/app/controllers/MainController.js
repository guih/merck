angular
	.module('Merck')
	.controller('MainController', MainController);

MainController.$inject = [ 
'$interval',
    '$window',
	'$scope', 
	'$timeout',
    '$state',
    '$ionicSideMenuDelegate',
    '$rootScope',
    'User',
];
function MainController($interval, $window, $scope, $timeout, $state, $ionicSideMenuDelegate, $rootScope, User){
    $scope.user = {};
             $scope.isRegister = null;

             $scope.register = {};
             $scope.login = {};

    console.log($state);
    $timeout(function(){
        if( $state.current.name ==  '/splash' ){

         $state.go('/initial', {location: 'replace'})
        }
    },2000)

    $scope.goTo = function(path){
        $state.go(path, {location: 'replace'})
    }


    $scope.toggleRegister =  function(){
        if( $scope.isRegister ){
             $scope.isRegister = null;
             return;
        }
        $scope.isRegister = true;

    }

    $scope.$watch('register', function(newValue, oldValue) {
        console.log('hey, REGOSTER has changed!', newValue, oldValue);
    });
    $scope.$watch('login', function(newValue, oldValue) {
        console.log('hey, login has changed!', newValue, oldValue);
    });    

    $scope.doRegister = function(){
        console.log('->', $scope.register)
        User.register( $scope.register ).then(function(res){
            console.log(res);
            $scope.notValidR  = null;
            $scope.isValidR = res.data;
            $scope.toggleRegister()  
            

        }, function(err){

            $scope.isValidR = null;


            $scope.notValidR = err.data;
        });       


    }

    $scope.doLogin = function(){
        console.log('->', $scope.login)


        User.login( $scope.login ).then(function(res){
            console.log(res);
            $scope.notValidL  = null;
            $scope.isValidL = res.data;
            $rootScope.backData = res.data.users;

            $scope.goTo('/admin-users');

        }, function(err){

            console.log('!!!!!!!!!!!!', err);
            $scope.isValidL = null;


            $scope.notValidL = err.data;
        });


    }   





    $scope.selections = function () {
        return $scope.contacts.filter(function (contact) {
            return contact.selected;
        })
    }
    
    $scope.dataTableOptions = {
       
        columns: [
            { title: "Nome" },
            { title: "E-mail" },
            { title: "Data de nascimento" },
            { title: "Tipo" },
            {title: 'Opções'}
        ],
        columnMap: function (p) { 
            return [ 
            
                p.meta.data.display_name, 
                p.meta.data.user_email, 
                p.dob || 'N/A', 
                p.type ||'Usuário', 

                '<a href="#/edit/'+ p.id+'" ng-click="setView()">Info</a>'
            ];
        },
"language": {
        "sProcessing":    "Procesando...",
        "sLengthMenu":    "Mostrar _MENU_ registros",
        "sZeroRecords":   "Sem resultados",
        "sEmptyTable":    "Nada encontrado",
        "sInfo":          "Mostrando registros de _START_ à _END_  de _TOTAL_ registros",
        "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":  "(filtrado de um total de _MAX_ registros)",
        "sInfoPostFix":   "",
        "sSearch":        "Buscar:",
        "sUrl":           "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Carregando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":    "Último",
            "sNext":    "Seguinte",
            "sPrevious": "Anterior"
        },

    },     
        columnDefs: [ {
            orderable: true,
            sortable: true,
            aTargets: [0, 1]
        } ],
        // order: [[ 1, 'asc' ]]
    };


    angular.element(document).ready(function() {
        var bootElement = document.getElementById("tableExample");
        angular.bootstrap(bootElement, ['Merck']);
    });

};