angular
	.module('Merck')
	.controller('AdminController', AdminController);

AdminController.$inject = [ 
    '$interval',
    '$window',
	'$scope', 
	'$timeout',
    '$state',
    '$ionicSideMenuDelegate',
    '$rootScope',
    'User'
];

function AdminController($interval, $window, $scope, $timeout, $state, $ionicSideMenuDelegate, $rootScope, User){
    $scope.user = {};

    $scope.goTo = function(path){
        $state.go(path, {location: 'replace'})
    }

    $scope.setView = function(){
        $scope.isView = true;
        $scope.isEdit = true;
    }


};