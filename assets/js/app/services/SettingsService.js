
angular
    .module('Merck')
        .factory('Settings', Settings);
Settings.$inject = [
    '$q', 
    '$http',
    '$window'
];

function Settings($q, $http, $window) {
	var Settings = {};

	Settings.API_BASE = window.Merck.ajax_url;
	return Settings;
};