angular
    .module('Merck')
        .factory('QuizService', QuizService);
QuizService.$inject = [
    '$q', 
    '$http',
    '$window',
    'Settings',
    '$state',
    
];

function QuizService($q, $http, $window, Settings) {
	var Quiz = {};
	Quiz.data =[]
	

	Settings.API_BASE = Settings.API_BASE || window.Merck.ajax_url;


	Quiz.fetch = function(data){
		if( !data )
			data  ={}		

		return $http.get(Settings.API_BASE+'?action=get_quiz',jQuery.param(data));
	};

	Quiz.saveAnswers = function(data){
		data.action = 'save_answers';

		return $http.post(Settings.API_BASE,jQuery.param(data), {
		    headers:
		    {
		        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
		    }
		});		
	}

	Quiz.setQuiz = function(data){

		console.log('>>>>>>>', data, Quiz.data);

		data.forEach(function(val, indx){

			Quiz.data.push(val);
		})

		Quiz.data = Quiz.data.filter(function(value, index){ return Quiz.data.indexOf(value) == index });
		return;
	
	}

	Quiz.getQuiz = function(){
		return Quiz.data || {};
	}

	Quiz.popQuiz = function(data){
		return Quiz.data.slice(-5);
	}





	return Quiz;
};