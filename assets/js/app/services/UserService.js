angular
    .module('Merck')
        .factory('User', User);
User.$inject = [
    '$q', 
    '$http',
    '$window',
    'Settings',
    '$state',
    
];

function User($q, $http, $window, Settings) {
	var User = {};

	Settings.API_BASE = Settings.API_BASE || window.Merck.ajax_url;
	User.login = function(data){
		data.action = 'login_wpw';

		return $http.post(Settings.API_BASE,jQuery.param(data), {
		    headers:
		    {
		        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
		    }
		});
	};

	User.register = function(data){
		data.action = 'register_user';

		return $http.post(Settings.API_BASE,jQuery.param(data), {
		    headers:
		    {
		        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
		    }
		});
	};	


	User.loginDob = function(data){
		data.action = 'login_dob';

		return $http.post(Settings.API_BASE,jQuery.param(data), {
		    headers:
		    {
		        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
		    }
		});
	}



	User.setUser = function(user){
		$window.localStorage.setItem('user', user);
	};
	User.getUser = function(){
		console.log('get user?')
		var user = JSON.parse($window.localStorage.getItem('user'));

		return user;
	};

	User.fetchUser = function(id){
	
			var data = {};


		data.action = 'fetch_user';
		data.id = id;

		return $http.post(Settings.API_BASE,jQuery.param(data), {
		    headers:
		    {
		        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
		    }
		});
	};

	
	User.erase = function(fn){
		$window.localStorage.removeItem('user');
		fn();
	}


	return User;
};