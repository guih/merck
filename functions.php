<?php 
global $session;
function register_session(){
    if( !session_id() )
        session_start();
}
add_action('init','register_session');
// login
$i = 0;
$quizyy = '[{"question":"1. Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Adequa sua linguagem verbal (escrita e falada) de forma simples e objetiva, sendo compreendido de forma fácil pelo cliente.","val":0,"id":1},{"caption":"Conhece a realidade de cada cliente, mantendo um histórico cronológico de informações e acompanhamento da evolução da conta.","val":0,"id":1}]},{"question":"2. Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Age de forma transparente e se comunica de forma clara com todos, não gerando interpretações duvidosas.","val":0,"id":2},{"caption":"Conhece os objetivos comerciais e financeiros dos clientes, entendo sua evolução e propondo melhorias se necessário.","val":0,"id":2}]},{"question":"3. Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"É aberto e disponível sempre que necessário, garantindo que as conversas sejam feitas naturalmente no dia a dia e não gerem \"gargalos\" e/ou \"ruídos\" de comunicação.","val":0,"id":3},{"caption":"Acompanha a operação dos clientes, entendendo sua forma de gestão, estrutura e organização da rotina diária.","val":0,"id":3}]},{"question":"4. Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"É paciente para ouvir e compreender seu interlocutor, fazendo inclusive perguntas para se aprofundar na linha de raciocínio do outro.","val":0,"id":4},{"caption":"Consegue diferenciar atendimento e relacionamento, tendo uma relação de confiança bem definida com os clientes e sendo visto por ele como um verdadeiro parceiro.","val":0,"id":4}]},{"question":"5. Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Compartilha o conhecimento e as informações não confidenciais de forma proativa com todos que possam se beneficiar delas.","val":0,"id":5},{"caption":"Consegue diferenciar uma opinião de uma sugestão de um cliente, endereçando internamente sempre que for pertinente tal recomendação.","val":0,"id":5}]},{"question":"* 6.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Pensa e elabora sua comunicação, exemplo: apresentações, de modo que transmita um bom encadeamento de ideias, atingindo seu objetivo.","val":0,"id":"295468766_1973309202"},{"caption":"Conhece bem os clientes e consegue antever possíveis desafios e riscos que podem atrapalhar os objetivos comerciais/financeiros mútuos.","val":0,"id":"295468766_1973309203"}]},{"question":"* 7.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Possui empatia e consegue se adaptar ao interlocutor com que se refere a sua forma de pensar e sentir.","val":0,"id":"295468767_1973309206"},{"caption":"Escuta ativamente, transmitindo sempre interesse pelo conteúdo e necessidades necessidades, sugestões e feedbacks do cliente.","val":0,"id":"295468767_1973309207"}]},{"question":"* 8.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Reflete antes de se comunicar de forma mais formal, exemplos: e-mail, reuniões, se está utilizando a melhor abordagem, criando uma \"história\" que envolva e seja de fácil compreensão para a audiência.","val":0,"id":"295468768_1973309210"},{"caption":"Mantém regularidade em reuniões formais, com pautas relevantes para a empresa e cliente, conseguindo sempre melhorar nosso relacionamento a partir desses encontros.","val":0,"id":"295468768_1973309211"}]},{"question":"* 9.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Utiliza as emoções positivamente na sua forma de comunicação, trazendo sempre um equilíbrio entre o desafio e o suporte.","val":0,"id":"295468769_1973309214"},{"caption":"Conhece bem os clientes e consegue antever possíveis desafios e riscos que podem atrapalhar os objetivos comerciais/financeiros mútuos.","val":0,"id":"295468769_1973309215"}]},{"question":"* 10.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Conhece os objetivos comerciais e financeiros dos clientes, entendo sua evolução e propondo melhorias se necessário.","val":0,"id":"295468770_1973309218"},{"caption":"Conhece o perfil dos principais clientes e consegue perceber em suas análises mudanças de comportamentos de consumo, identificando se é algo pontual ou um indicativo de queda no volume de compras do cliente.","val":0,"id":"295468770_1973309219"}]},{"question":"* 11.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Possui inteligência emocional para não desanimar e/ou reclamar das ações que não atingiram seu objetivo.","val":0,"id":"295468771_1973309222"},{"caption":"Planeja novas ações para o aumento do sell out, defendendo inclusive possíveis investimentos se necessários.","val":0,"id":"295468771_1973309223"}]},{"question":"* 12.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Argumenta-se de forma objetiva e consegue fazer o interlocutor refletir sobre suas próprias necessidades e possíveis ganhos durante a conversa.","val":0,"id":"295468772_1973309226"},{"caption":"Cria argumentos e condições que viabilizam a retomada do poder de compra do cliente. É flexível em sair do seu padrão de vendas para se adaptar a realidade do cliente, não comprometendo as regras e processos internos.","val":0,"id":"295468772_1973309227"}]},{"question":"* 13.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Possui foco na solução e não no problema, mantendo uma postura otimista e flexível.","val":0,"id":"295468773_1973309230"},{"caption":"Traz mediante a um problema as possibilidades de resolvê-lo, se mantendo aberto a ouvir outras ideias também.","val":0,"id":"295468773_1973309231"}]},{"question":"* 14.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Possui argumentos que focam em benefícios e vantagens, e não somente em características e preço promocional.","val":0,"id":"295468774_1973309234"},{"caption":"Possui uma boa rede de relacionamento que ajuda tanto dentro como fora da empresa para conquistar os objetivos.","val":0,"id":"295468774_1973309235"}]},{"question":"* 15.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Propõe e ajusta condições comerciais que trazem o nosso diferencial em relação a concorrência.","val":0,"id":"295468775_1973309238"},{"caption":"Planeja atividades especificando o que, como e o por que priorizar tais ações no tempo correto.","val":0,"id":"295468775_1973309239"}]},{"question":"* 16.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Domina as ferramentas (Roambi, Pharmalink, outras) de forma que consiga utilizar para melhorar suas análises, recomendações e decisões.","val":0,"id":"295468776_1973309242"},{"caption":"Propõe serviços que sejam de interesse daqueles que tem o poder de decidir durante uma negociação, mostrando nosso diferencial.","val":0,"id":"295468776_1973309243"}]},{"question":"* 17.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Garante que as campanhas estarão dentro das regras de conformidade da empresa (compliance) e terão responsáveis na ponta para garantir a correta execução.","val":0,"id":"295468777_1973309246"},{"caption":"Possui informações personalizadas de cada cliente que são utilizadas sistematicamente na forma de comunicar e negociar.","val":0,"id":"295468777_1973309247"}]},{"question":"* 18.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Reverte bem produtos de baixo giro, fazendo ações específicas e mensuráveis para não gerar possíveis prejuízos de perda de estoque e rentabilidade.","val":0,"id":"295468778_1973309250"},{"caption":"Conhece e mantém um relacionamento próximo com os principais influenciadores que de alguma forma impactam no sucesso do negócio, tanto interno (Merck), como no próprio estabelecimento do cliente.","val":0,"id":"295468778_1973309251"}]},{"question":"* 19.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Propõe junto aos clientes ações, exemplo: uma campanha, sendo muito coerente com o foco e as metas da empresa.","val":0,"id":"295468779_1973309254"},{"caption":"Consegue correlacionar na sua estratégia de gestão de contas com cada interlocutor, sabendo quem e como abordar cada um dependendo do tema/produto.","val":0,"id":"295468779_1973309255"}]},{"question":"* 20.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Domina as ferramentas (Roambi, Pharmalink, outras) de forma que consiga utilizar para melhorar suas análises, recomendações e decisões.","val":0,"id":"295468780_1973309258"},{"caption":"Possui informações que cruza possíveis riscos com o perfil do interlocutor que tem influência e poder de decisão.","val":0,"id":"295468780_1973309259"}]},{"question":"* 21.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Segue a rotina de \"campo\" para identificar novas formas de fazer as coisas, seja um processo, procedimento ou uma prática de trabalho.","val":0,"id":"295468781_1973309262"},{"caption":"Possui mapeado de forma estruturada todos os interlocutores da qual converso e negocio, sabendo o grau de influência e poder de decisão de cada um.","val":0,"id":"295468781_1973309263"}]},{"question":"* 22.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Busca suporte para tomar decisões mais complexas e que fogem sua autonomia e responsabilidades.","val":0,"id":"295468782_1973309266"},{"caption":"Adequa sua linguagem verbal (escrita e falada) de forma simples e objetiva, sendo compreendido de forma fácil pelo cliente.","val":0,"id":"295468782_1973309267"}]},{"question":"* 23.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Conhece bem as informações técnicas e argumentos de vendas do produto (benefícios e diferenciais), sendo sensível sempre a sazonalidade do mercado e às grades promocionais vigentes.","val":0,"id":"295468783_1973309270"},{"caption":"Age de forma transparente e se comunica de forma clara com todos, não gerando interpretações duvidosas.","val":0,"id":"295468783_1973309271"}]},{"question":"* 24.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Tem disciplina e senso de organização do seu trabalho para cumprir prazos, orçamentos e regras internas.","val":0,"id":"295468784_1973309274"},{"caption":"É aberto e disponível sempre que necessário, garantindo que as conversas sejam feitas naturalmente no dia a dia e não gerem \"gargalos\" e/ou \"ruídos\" de comunicação.","val":0,"id":"295468784_1973309275"}]},{"question":"* 25.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Possui um forma de gerenciar suas contas, garantindo que o cliente seja atendido, tendo sempre em mente que o tempo e os recursos devem ser proporcionais ao potencial retorno desse investimento.","val":0,"id":"295468785_1973309278"},{"caption":"É paciente para ouvir e compreender seu interlocutor, fazendo inclusive perguntas para se aprofundar na linha de raciocínio do outro.","val":0,"id":"295468785_1973309279"}]},{"question":"* 26.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Gerencio algumas exceções , garantindo que o cliente seja atendido, tendo sempre em mente o equilíbrio entre os processos internos e a flexibilização possível.","val":0,"id":"295468786_1973309282"},{"caption":"Compartilha o conhecimento e as informações não confidenciais de forma proativa com todos que possam se beneficiar delas.","val":0,"id":"295468786_1973309283"}]},{"question":"* 27.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Tem disciplina e senso de organização do seu trabalho para cumprir prazos, orçamentos e regras internas.","val":0,"id":"295468787_1973309286"},{"caption":"Pensa e elabora sua comunicação, exemplo: apresentações, de modo que transmita um bom encadeamento de ideias, atingindo seu objetivo.","val":0,"id":"295468787_1973309287"}]},{"question":"* 28.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Busca suporte para tomar decisões mais complexas e que fogem sua autonomia e responsabilidades.","val":0,"id":"295468788_1973309290"},{"caption":"Possui empatia e consegue se adaptar ao interlocutor com que se refere a sua forma de pensar e sentir.","val":0,"id":"295468788_1973309291"}]},{"question":"* 29.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Conhece bem as informações técnicas e argumentos de vendas do produto (benefícios e diferenciais), sendo sensível sempre a sazonalidade do mercado e às grades promocionais vigentes.","val":0,"id":"295468789_1973309294"},{"caption":"Reflete antes de se comunicar de forma mais formal, exemplos: e-mail, reuniões, se está utilizando a melhor abordagem, criando uma \"história\" que envolva e seja de fácil compreensão para a audiência.","val":0,"id":"295468789_1973309295"}]},{"question":"* 30.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Apresenta motivação mesmo em situações de conflitos, sendo resiliente e agindo racionalmente.","val":0,"id":"295468790_1973309298"},{"caption":"Conhece a realidade de cada cliente, mantendo um histórico cronológico de informações e acompanhamento da evolução da conta.","val":0,"id":"295468790_1973309299"}]},{"question":"* 31.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Argumenta-se de forma objetiva e consegue fazer o interlocutor refletir sobre suas próprias necessidades e possíveis ganhos durante a conversa.","val":0,"id":"295468791_1973309302"},{"caption":"Escuta ativamente, transmitindo sempre interesse pelo conteúdo e necessidades necessidades, sugestões e feedbacks do cliente.","val":0,"id":"295468791_1973309303"}]},{"question":"* 32.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Segue a rotina de \"campo\" para identificar novas formas de fazer as coisas, seja um processo, procedimento ou uma prática de trabalho.","val":0,"id":"295468792_1973309306"},{"caption":"Acompanha a operação dos clientes, entendendo sua forma de gestão, estrutura e organização da rotina diária.","val":0,"id":"295468792_1973309307"}]},{"question":"* 33.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Possui argumentos que focam em benefícios e vantagens, e não somente em características e preço promocional.","val":0,"id":"295468793_1973309310"},{"caption":"Consegue diferenciar atendimento e relacionamento, tendo uma relação de confiança bem definida com os clientes e sendo visto por ele como um verdadeiro parceiro.","val":0,"id":"295468793_1973309311"}]},{"question":"* 34.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Propõe e ajusta condições comerciais que trazem o nosso diferencial em relação a concorrência.","val":0,"id":"295468794_1973309314"},{"caption":"Consegue diferenciar uma opinião de uma sugestão de um cliente, endereçando internamente sempre que for pertinente tal recomendação.","val":0,"id":"295468794_1973309315"}]},{"question":"* 35.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"É visto como um líder referência em seus comportamentos, mantendo uma relação saudável com seu time.","val":0,"id":"295468795_1973309329"},{"caption":"Conhece o perfil dos principais clientes e consegue perceber em suas análises mudanças de comportamentos de consumo, identificando se é algo pontual ou um indicativo de queda no volume de compras do cliente.","val":0,"id":"295468795_1973309330"}]},{"question":"* 36.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Confia na maioria das vezes, dando sempre mais autonomia e responsabilidades para pessoas da minha equipe que percebo estarem preparadas.","val":0,"id":"295468796_1973309333"},{"caption":"Possui informações personalizadas de cada cliente que são utilizadas sistematicamente na forma de comunicar e negociar.","val":0,"id":"295468796_1973309334"}]},{"question":"* 37.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Conhece os pontos fortes e fracos de cada um do time, mantendo conversas mais claras para o desenvolvimento individual.","val":0,"id":"295468797_1973309337"},{"caption":"Possui mapeado de forma estruturada todos os interlocutores da qual converso e negocio, sabendo o grau de influência e poder de decisão de cada um.","val":0,"id":"295468797_1973309338"}]},{"question":"* 38.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Mantém um ambiente de confiança e transparência, não tendo em sua gestão problemas relacionados a fofocas ou competitividade que segrega o time.","val":0,"id":"295468798_1973309341"},{"caption":"Possui uma boa rede de relacionamento que ajuda tanto dentro como fora da empresa para conquistar os objetivos.","val":0,"id":"295468798_1973309342"}]},{"question":"* 39.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Dimensiona para sua equipe as tarefas, desafios e objetivos de acordo com as capacidades individuais, equilibrando o \"sair da zona de conforto\" com \"desafios de aprendizagem\".","val":0,"id":"295468799_1973309345"},{"caption":"Planeja novas ações para o aumento do sell out, defendendo inclusive possíveis investimentos se necessários.","val":0,"id":"295468799_1973309346"}]},{"question":"* 40.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"É visto como um líder referência em seus comportamentos, mantendo uma relação saudável com seu time.","val":0,"id":"295468800_1973309349"},{"caption":"Planeja atividades especificando o que, como e o por que priorizar tais ações no tempo correto.","val":0,"id":"295468800_1973309350"}]},{"question":"* 41.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Em seus feedbacks, consegue deixar claro o que é esperado de cada um do seu time, inclusive os desafiando por meio de objetivos individuais.","val":0,"id":"295468801_1973309353"},{"caption":"Possui um forma de gerenciar suas contas, garantindo que o cliente seja atendido, tendo sempre em mente que o tempo e os recursos devem ser proporcionais ao potencial retorno desse investimento.","val":0,"id":"295468801_1973309354"}]},{"question":"* 42.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Confia na maioria das vezes, dando sempre mais autonomia e responsabilidades para pessoas da minha equipe que percebo estarem preparadas.","val":0,"id":"295468802_1973309357"},{"caption":"Garante que as campanhas estarão dentro das regras de conformidade da empresa (compliance) e terão responsáveis na ponta para garantir a correta execução.","val":0,"id":"295468802_1973309358"}]},{"question":"* 43.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Viabiliza que todos tenham as mesmas oportunidades de aprendizado, seja por meio de incentivo ao autodesenvolvimento ou a troca entre pessoas com experiências profissionais diferentes, exemplo: job rotation.","val":0,"id":"295468803_1973309361"},{"caption":"Cria argumentos e condições que viabilizam a retomada do poder de compra do cliente. É flexível em sair do seu padrão de vendas para se adaptar a realidade do cliente, não comprometendo as regras e processos internos.","val":0,"id":"295468803_1973309362"}]},{"question":"* 44.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Utiliza as emoções positivamente na sua forma de comunicação, trazendo sempre um equilíbrio entre o desafio e o suporte.","val":0,"id":"295468804_1973309365"},{"caption":"Propõe junto aos clientes ações, exemplo: uma campanha, sendo muito coerente com o foco e as metas da empresa.","val":0,"id":"295468804_1973309366"}]},{"question":"* 45.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Mantém um ambiente de confiança e transparência, não tendo em sua gestão problemas relacionados a fofocas ou competitividade que segrega o time.","val":0,"id":"295468805_1973309369"},{"caption":"Possui inteligência emocional para não desanimar e/ou reclamar das ações que não atingiram seu objetivo.","val":0,"id":"295468805_1973309370"}]},{"question":"* 46.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Apresenta motivação mesmo em situações de conflitos, sendo resiliente e agindo racionalmente.","val":0,"id":"295468806_1973309373"},{"caption":"Em seus feedbacks, consegue deixar claro o que é esperado de cada um do seu time, inclusive os desafiando por meio de objetivos individuais.","val":0,"id":"295468806_1973309374"}]},{"question":"* 47.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Sou consciente dos interlocutores que tenho maior dificuldade em manter um relacionamento mais próximo e busco sempre novas formas de aproximação.","val":0,"id":"295468807_1973309377"},{"caption":"Dimensiona para sua equipe as tarefas, desafios e objetivos de acordo com as capacidades individuais, equilibrando o \"sair da zona de conforto\" com \"desafios de aprendizagem\".","val":0,"id":"295468807_1973309378"}]},{"question":"* 48.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Consegue correlacionar na sua estratégia de gestão de contas com cada interlocutor, sabendo quem e como abordar cada um dependendo do tema/produto.","val":0,"id":"295468808_1973309381"},{"caption":"Viabiliza que todos tenham as mesmas oportunidades de aprendizado, seja por meio de incentivo ao autodesenvolvimento ou a troca entre pessoas com experiências profissionais diferentes, exemplo: job rotation.","val":0,"id":"295468808_1973309382"}]},{"question":"* 49.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Possui informações que cruza possíveis riscos com o perfil do interlocutor que tem influência e poder de decisão.","val":0,"id":"295468809_1973309385"},{"caption":"Possui foco na solução e não no problema, mantendo uma postura otimista e flexível.","val":0,"id":"295468809_1973309386"}]},{"question":"* 50.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.","options":[{"caption":"Gerencio algumas exceções , garantindo que o cliente seja atendido, tendo sempre em mente o equilíbrio entre os processos internos e a flexibilização possível.","val":0,"id":"295468810_1973309389"},{"caption":"Conhece os pontos fortes e fracos de cada um do time, mantendo conversas mais claras para o desenvolvimento individual.","val":0,"id":"295468810_1973309390"}]},{"question":"* 51.* Agora você vai escolher 10 temas que você identifica como necessários para o seu desenvolvimento. Ou seja, quais dos assuntos abaixo você considera importante para aprender mais.","options":[{"caption":"Empreendedorismo","val":0,"id":"295468754_1973309109"},{"caption":"Negociação","val":0,"id":"295468754_1973309110"}]},{"question":"* 52.* Para finalizar, você vai escolher 10 capacidades que você identifica como seus pontos fortes em termos de competências.","options":[{"caption":"Capacidade de identificar oportunidades e fazer negócios.","val":0,"id":"295468755_1973309146"},{"caption":"Capacidade de convergir interesses, beneficiando ambas as partes.","val":0,"id":"295468755_1973309147"}]}]';
$quiz = '[{
		"question": "1. Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Adequa sua linguagem verbal (escrita e falada) de forma simples e objetiva, sendo compreendido de forma fácil pelo cliente.",
			"val": 0,
			"id": 1
		}, {
			"caption": "Conhece a realidade de cada cliente, mantendo um histórico cronológico de informações e acompanhamento da evolução da conta.",
			"val": 0,
			"id": 1
		}]
	}, {
		"question": "2. Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Age de forma transparente e se comunica de forma clara com todos, não gerando interpretações duvidosas.",
			"val": 0,
			"id": 2
		}, {
			"caption": "Conhece os objetivos comerciais e financeiros dos clientes, entendo sua evolução e propondo melhorias se necessário.",
			"val": 0,
			"id": 2
		}]
	}, {
		"question": "3. Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "É aberto e disponível sempre que necessário, garantindo que as conversas sejam feitas naturalmente no dia a dia e não gerem \"gargalos\" e/ou \"ruídos\" de comunicação.",
			"val": 0,
			"id": 3
		}, {
			"caption": "Acompanha a operação dos clientes, entendendo sua forma de gestão, estrutura e organização da rotina diária.",
			"val": 0,
			"id": 3
		}]
	}, {
		"question": "4. Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "É paciente para ouvir e compreender seu interlocutor, fazendo inclusive perguntas para se aprofundar na linha de raciocínio do outro.",
			"val": 0,
			"id": 4
		}, {
			"caption": "Consegue diferenciar atendimento e relacionamento, tendo uma relação de confiança bem definida com os clientes e sendo visto por ele como um verdadeiro parceiro.",
			"val": 0,
			"id": 4
		}]
	}, {
		"question": "5. Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Compartilha o conhecimento e as informações não confidenciais de forma proativa com todos que possam se beneficiar delas.",
			"val": 0,
			"id": 5
		}, {
			"caption": "Consegue diferenciar uma opinião de uma sugestão de um cliente, endereçando internamente sempre que for pertinente tal recomendação.",
			"val": 0,
			"id": 5
		}]
	}, {
		"question": "* 6.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Pensa e elabora sua comunicação, exemplo: apresentações, de modo que transmita um bom encadeamento de ideias, atingindo seu objetivo.",
			"val": 0,
			"id": "295468766_1973309202"
		}, {
			"caption": "Conhece bem os clientes e consegue antever possíveis desafios e riscos que podem atrapalhar os objetivos comerciais/financeiros mútuos.",
			"val": 0,
			"id": "295468766_1973309203"
		}]
	}, {
		"question": "* 7.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Possui empatia e consegue se adaptar ao interlocutor com que se refere a sua forma de pensar e sentir.",
			"val": 0,
			"id": "295468767_1973309206"
		}, {
			"caption": "Escuta ativamente, transmitindo sempre interesse pelo conteúdo e necessidades necessidades, sugestões e feedbacks do cliente.",
			"val": 0,
			"id": "295468767_1973309207"
		}]
	}, {
		"question": "* 8.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Reflete antes de se comunicar de forma mais formal, exemplos: e-mail, reuniões, se está utilizando a melhor abordagem, criando uma \"história\" que envolva e seja de fácil compreensão para a audiência.",
			"val": 0,
			"id": "295468768_1973309210"
		}, {
			"caption": "Mantém regularidade em reuniões formais, com pautas relevantes para a empresa e cliente, conseguindo sempre melhorar nosso relacionamento a partir desses encontros.",
			"val": 0,
			"id": "295468768_1973309211"
		}]
	}, {
		"question": "* 9.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Utiliza as emoções positivamente na sua forma de comunicação, trazendo sempre um equilíbrio entre o desafio e o suporte.",
			"val": 0,
			"id": "295468769_1973309214"
		}, {
			"caption": "Conhece bem os clientes e consegue antever possíveis desafios e riscos que podem atrapalhar os objetivos comerciais/financeiros mútuos.",
			"val": 0,
			"id": "295468769_1973309215"
		}]
	}, {
		"question": "* 10.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Conhece os objetivos comerciais e financeiros dos clientes, entendo sua evolução e propondo melhorias se necessário.",
			"val": 0,
			"id": "295468770_1973309218"
		}, {
			"caption": "Conhece o perfil dos principais clientes e consegue perceber em suas análises mudanças de comportamentos de consumo, identificando se é algo pontual ou um indicativo de queda no volume de compras do cliente.",
			"val": 0,
			"id": "295468770_1973309219"
		}]
	}, {
		"question": "* 11.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Possui inteligência emocional para não desanimar e/ou reclamar das ações que não atingiram seu objetivo.",
			"val": 0,
			"id": "295468771_1973309222"
		}, {
			"caption": "Planeja novas ações para o aumento do sell out, defendendo inclusive possíveis investimentos se necessários.",
			"val": 0,
			"id": "295468771_1973309223"
		}]
	}, {
		"question": "* 12.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Argumenta-se de forma objetiva e consegue fazer o interlocutor refletir sobre suas próprias necessidades e possíveis ganhos durante a conversa.",
			"val": 0,
			"id": "295468772_1973309226"
		}, {
			"caption": "Cria argumentos e condições que viabilizam a retomada do poder de compra do cliente. É flexível em sair do seu padrão de vendas para se adaptar a realidade do cliente, não comprometendo as regras e processos internos.",
			"val": 0,
			"id": "295468772_1973309227"
		}]
	}, {
		"question": "* 13.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Possui foco na solução e não no problema, mantendo uma postura otimista e flexível.",
			"val": 0,
			"id": "295468773_1973309230"
		}, {
			"caption": "Traz mediante a um problema as possibilidades de resolvê-lo, se mantendo aberto a ouvir outras ideias também.",
			"val": 0,
			"id": "295468773_1973309231"
		}]
	}, {
		"question": "* 14.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Possui argumentos que focam em benefícios e vantagens, e não somente em características e preço promocional.",
			"val": 0,
			"id": "295468774_1973309234"
		}, {
			"caption": "Possui uma boa rede de relacionamento que ajuda tanto dentro como fora da empresa para conquistar os objetivos.",
			"val": 0,
			"id": "295468774_1973309235"
		}]
	}, {
		"question": "* 15.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Propõe e ajusta condições comerciais que trazem o nosso diferencial em relação a concorrência.",
			"val": 0,
			"id": "295468775_1973309238"
		}, {
			"caption": "Planeja atividades especificando o que, como e o por que priorizar tais ações no tempo correto.",
			"val": 0,
			"id": "295468775_1973309239"
		}]
	}, {
		"question": "* 16.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Domina as ferramentas (Roambi, Pharmalink, outras) de forma que consiga utilizar para melhorar suas análises, recomendações e decisões.",
			"val": 0,
			"id": "295468776_1973309242"
		}, {
			"caption": "Propõe serviços que sejam de interesse daqueles que tem o poder de decidir durante uma negociação, mostrando nosso diferencial.",
			"val": 0,
			"id": "295468776_1973309243"
		}]
	}, {
		"question": "* 17.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Garante que as campanhas estarão dentro das regras de conformidade da empresa (compliance) e terão responsáveis na ponta para garantir a correta execução.",
			"val": 0,
			"id": "295468777_1973309246"
		}, {
			"caption": "Possui informações personalizadas de cada cliente que são utilizadas sistematicamente na forma de comunicar e negociar.",
			"val": 0,
			"id": "295468777_1973309247"
		}]
	}, {
		"question": "* 18.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Reverte bem produtos de baixo giro, fazendo ações específicas e mensuráveis para não gerar possíveis prejuízos de perda de estoque e rentabilidade.",
			"val": 0,
			"id": "295468778_1973309250"
		}, {
			"caption": "Conhece e mantém um relacionamento próximo com os principais influenciadores que de alguma forma impactam no sucesso do negócio, tanto interno (Merck), como no próprio estabelecimento do cliente.",
			"val": 0,
			"id": "295468778_1973309251"
		}]
	}, {
		"question": "* 19.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Propõe junto aos clientes ações, exemplo: uma campanha, sendo muito coerente com o foco e as metas da empresa.",
			"val": 0,
			"id": "295468779_1973309254"
		}, {
			"caption": "Consegue correlacionar na sua estratégia de gestão de contas com cada interlocutor, sabendo quem e como abordar cada um dependendo do tema/produto.",
			"val": 0,
			"id": "295468779_1973309255"
		}]
	}, {
		"question": "* 20.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Domina as ferramentas (Roambi, Pharmalink, outras) de forma que consiga utilizar para melhorar suas análises, recomendações e decisões.",
			"val": 0,
			"id": "295468780_1973309258"
		}, {
			"caption": "Possui informações que cruza possíveis riscos com o perfil do interlocutor que tem influência e poder de decisão.",
			"val": 0,
			"id": "295468780_1973309259"
		}]
	}, {
		"question": "* 21.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Segue a rotina de \"campo\" para identificar novas formas de fazer as coisas, seja um processo, procedimento ou uma prática de trabalho.",
			"val": 0,
			"id": "295468781_1973309262"
		}, {
			"caption": "Possui mapeado de forma estruturada todos os interlocutores da qual converso e negocio, sabendo o grau de influência e poder de decisão de cada um.",
			"val": 0,
			"id": "295468781_1973309263"
		}]
	}, {
		"question": "* 22.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Busca suporte para tomar decisões mais complexas e que fogem sua autonomia e responsabilidades.",
			"val": 0,
			"id": "295468782_1973309266"
		}, {
			"caption": "Adequa sua linguagem verbal (escrita e falada) de forma simples e objetiva, sendo compreendido de forma fácil pelo cliente.",
			"val": 0,
			"id": "295468782_1973309267"
		}]
	}, {
		"question": "* 23.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Conhece bem as informações técnicas e argumentos de vendas do produto (benefícios e diferenciais), sendo sensível sempre a sazonalidade do mercado e às grades promocionais vigentes.",
			"val": 0,
			"id": "295468783_1973309270"
		}, {
			"caption": "Age de forma transparente e se comunica de forma clara com todos, não gerando interpretações duvidosas.",
			"val": 0,
			"id": "295468783_1973309271"
		}]
	}, {
		"question": "* 24.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Tem disciplina e senso de organização do seu trabalho para cumprir prazos, orçamentos e regras internas.",
			"val": 0,
			"id": "295468784_1973309274"
		}, {
			"caption": "É aberto e disponível sempre que necessário, garantindo que as conversas sejam feitas naturalmente no dia a dia e não gerem \"gargalos\" e/ou \"ruídos\" de comunicação.",
			"val": 0,
			"id": "295468784_1973309275"
		}]
	}, {
		"question": "* 25.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Possui um forma de gerenciar suas contas, garantindo que o cliente seja atendido, tendo sempre em mente que o tempo e os recursos devem ser proporcionais ao potencial retorno desse investimento.",
			"val": 0,
			"id": "295468785_1973309278"
		}, {
			"caption": "É paciente para ouvir e compreender seu interlocutor, fazendo inclusive perguntas para se aprofundar na linha de raciocínio do outro.",
			"val": 0,
			"id": "295468785_1973309279"
		}]
	}, {
		"question": "* 26.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Gerencio algumas exceções , garantindo que o cliente seja atendido, tendo sempre em mente o equilíbrio entre os processos internos e a flexibilização possível.",
			"val": 0,
			"id": "295468786_1973309282"
		}, {
			"caption": "Compartilha o conhecimento e as informações não confidenciais de forma proativa com todos que possam se beneficiar delas.",
			"val": 0,
			"id": "295468786_1973309283"
		}]
	}, {
		"question": "* 27.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Tem disciplina e senso de organização do seu trabalho para cumprir prazos, orçamentos e regras internas.",
			"val": 0,
			"id": "295468787_1973309286"
		}, {
			"caption": "Pensa e elabora sua comunicação, exemplo: apresentações, de modo que transmita um bom encadeamento de ideias, atingindo seu objetivo.",
			"val": 0,
			"id": "295468787_1973309287"
		}]
	}, {
		"question": "* 28.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Busca suporte para tomar decisões mais complexas e que fogem sua autonomia e responsabilidades.",
			"val": 0,
			"id": "295468788_1973309290"
		}, {
			"caption": "Possui empatia e consegue se adaptar ao interlocutor com que se refere a sua forma de pensar e sentir.",
			"val": 0,
			"id": "295468788_1973309291"
		}]
	}, {
		"question": "* 29.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Conhece bem as informações técnicas e argumentos de vendas do produto (benefícios e diferenciais), sendo sensível sempre a sazonalidade do mercado e às grades promocionais vigentes.",
			"val": 0,
			"id": "295468789_1973309294"
		}, {
			"caption": "Reflete antes de se comunicar de forma mais formal, exemplos: e-mail, reuniões, se está utilizando a melhor abordagem, criando uma \"história\" que envolva e seja de fácil compreensão para a audiência.",
			"val": 0,
			"id": "295468789_1973309295"
		}]
	}, {
		"question": "* 30.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Apresenta motivação mesmo em situações de conflitos, sendo resiliente e agindo racionalmente.",
			"val": 0,
			"id": "295468790_1973309298"
		}, {
			"caption": "Conhece a realidade de cada cliente, mantendo um histórico cronológico de informações e acompanhamento da evolução da conta.",
			"val": 0,
			"id": "295468790_1973309299"
		}]
	}, {
		"question": "* 31.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Argumenta-se de forma objetiva e consegue fazer o interlocutor refletir sobre suas próprias necessidades e possíveis ganhos durante a conversa.",
			"val": 0,
			"id": "295468791_1973309302"
		}, {
			"caption": "Escuta ativamente, transmitindo sempre interesse pelo conteúdo e necessidades necessidades, sugestões e feedbacks do cliente.",
			"val": 0,
			"id": "295468791_1973309303"
		}]
	}, {
		"question": "* 32.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Segue a rotina de \"campo\" para identificar novas formas de fazer as coisas, seja um processo, procedimento ou uma prática de trabalho.",
			"val": 0,
			"id": "295468792_1973309306"
		}, {
			"caption": "Acompanha a operação dos clientes, entendendo sua forma de gestão, estrutura e organização da rotina diária.",
			"val": 0,
			"id": "295468792_1973309307"
		}]
	}, {
		"question": "* 33.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Possui argumentos que focam em benefícios e vantagens, e não somente em características e preço promocional.",
			"val": 0,
			"id": "295468793_1973309310"
		}, {
			"caption": "Consegue diferenciar atendimento e relacionamento, tendo uma relação de confiança bem definida com os clientes e sendo visto por ele como um verdadeiro parceiro.",
			"val": 0,
			"id": "295468793_1973309311"
		}]
	}, {
		"question": "* 34.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Propõe e ajusta condições comerciais que trazem o nosso diferencial em relação a concorrência.",
			"val": 0,
			"id": "295468794_1973309314"
		}, {
			"caption": "Consegue diferenciar uma opinião de uma sugestão de um cliente, endereçando internamente sempre que for pertinente tal recomendação.",
			"val": 0,
			"id": "295468794_1973309315"
		}]
	}, {
		"question": "* 35.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "É visto como um líder referência em seus comportamentos, mantendo uma relação saudável com seu time.",
			"val": 0,
			"id": "295468795_1973309329"
		}, {
			"caption": "Conhece o perfil dos principais clientes e consegue perceber em suas análises mudanças de comportamentos de consumo, identificando se é algo pontual ou um indicativo de queda no volume de compras do cliente.",
			"val": 0,
			"id": "295468795_1973309330"
		}]
	}, {
		"question": "* 36.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Confia na maioria das vezes, dando sempre mais autonomia e responsabilidades para pessoas da minha equipe que percebo estarem preparadas.",
			"val": 0,
			"id": "295468796_1973309333"
		}, {
			"caption": "Possui informações personalizadas de cada cliente que são utilizadas sistematicamente na forma de comunicar e negociar.",
			"val": 0,
			"id": "295468796_1973309334"
		}]
	}, {
		"question": "* 37.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Conhece os pontos fortes e fracos de cada um do time, mantendo conversas mais claras para o desenvolvimento individual.",
			"val": 0,
			"id": "295468797_1973309337"
		}, {
			"caption": "Possui mapeado de forma estruturada todos os interlocutores da qual converso e negocio, sabendo o grau de influência e poder de decisão de cada um.",
			"val": 0,
			"id": "295468797_1973309338"
		}]
	}, {
		"question": "* 38.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Mantém um ambiente de confiança e transparência, não tendo em sua gestão problemas relacionados a fofocas ou competitividade que segrega o time.",
			"val": 0,
			"id": "295468798_1973309341"
		}, {
			"caption": "Possui uma boa rede de relacionamento que ajuda tanto dentro como fora da empresa para conquistar os objetivos.",
			"val": 0,
			"id": "295468798_1973309342"
		}]
	}, {
		"question": "* 39.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Dimensiona para sua equipe as tarefas, desafios e objetivos de acordo com as capacidades individuais, equilibrando o \"sair da zona de conforto\" com \"desafios de aprendizagem\".",
			"val": 0,
			"id": "295468799_1973309345"
		}, {
			"caption": "Planeja novas ações para o aumento do sell out, defendendo inclusive possíveis investimentos se necessários.",
			"val": 0,
			"id": "295468799_1973309346"
		}]
	}, {
		"question": "* 40.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "É visto como um líder referência em seus comportamentos, mantendo uma relação saudável com seu time.",
			"val": 0,
			"id": "295468800_1973309349"
		}, {
			"caption": "Planeja atividades especificando o que, como e o por que priorizar tais ações no tempo correto.",
			"val": 0,
			"id": "295468800_1973309350"
		}]
	}, {
		"question": "* 41.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Em seus feedbacks, consegue deixar claro o que é esperado de cada um do seu time, inclusive os desafiando por meio de objetivos individuais.",
			"val": 0,
			"id": "295468801_1973309353"
		}, {
			"caption": "Possui um forma de gerenciar suas contas, garantindo que o cliente seja atendido, tendo sempre em mente que o tempo e os recursos devem ser proporcionais ao potencial retorno desse investimento.",
			"val": 0,
			"id": "295468801_1973309354"
		}]
	}, {
		"question": "* 42.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Confia na maioria das vezes, dando sempre mais autonomia e responsabilidades para pessoas da minha equipe que percebo estarem preparadas.",
			"val": 0,
			"id": "295468802_1973309357"
		}, {
			"caption": "Garante que as campanhas estarão dentro das regras de conformidade da empresa (compliance) e terão responsáveis na ponta para garantir a correta execução.",
			"val": 0,
			"id": "295468802_1973309358"
		}]
	}, {
		"question": "* 43.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Viabiliza que todos tenham as mesmas oportunidades de aprendizado, seja por meio de incentivo ao autodesenvolvimento ou a troca entre pessoas com experiências profissionais diferentes, exemplo: job rotation.",
			"val": 0,
			"id": "295468803_1973309361"
		}, {
			"caption": "Cria argumentos e condições que viabilizam a retomada do poder de compra do cliente. É flexível em sair do seu padrão de vendas para se adaptar a realidade do cliente, não comprometendo as regras e processos internos.",
			"val": 0,
			"id": "295468803_1973309362"
		}]
	}, {
		"question": "* 44.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Utiliza as emoções positivamente na sua forma de comunicação, trazendo sempre um equilíbrio entre o desafio e o suporte.",
			"val": 0,
			"id": "295468804_1973309365"
		}, {
			"caption": "Propõe junto aos clientes ações, exemplo: uma campanha, sendo muito coerente com o foco e as metas da empresa.",
			"val": 0,
			"id": "295468804_1973309366"
		}]
	}, {
		"question": "* 45.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Mantém um ambiente de confiança e transparência, não tendo em sua gestão problemas relacionados a fofocas ou competitividade que segrega o time.",
			"val": 0,
			"id": "295468805_1973309369"
		}, {
			"caption": "Possui inteligência emocional para não desanimar e/ou reclamar das ações que não atingiram seu objetivo.",
			"val": 0,
			"id": "295468805_1973309370"
		}]
	}, {
		"question": "* 46.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Apresenta motivação mesmo em situações de conflitos, sendo resiliente e agindo racionalmente.",
			"val": 0,
			"id": "295468806_1973309373"
		}, {
			"caption": "Em seus feedbacks, consegue deixar claro o que é esperado de cada um do seu time, inclusive os desafiando por meio de objetivos individuais.",
			"val": 0,
			"id": "295468806_1973309374"
		}]
	}, {
		"question": "* 47.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Sou consciente dos interlocutores que tenho maior dificuldade em manter um relacionamento mais próximo e busco sempre novas formas de aproximação.",
			"val": 0,
			"id": "295468807_1973309377"
		}, {
			"caption": "Dimensiona para sua equipe as tarefas, desafios e objetivos de acordo com as capacidades individuais, equilibrando o \"sair da zona de conforto\" com \"desafios de aprendizagem\".",
			"val": 0,
			"id": "295468807_1973309378"
		}]
	}, {
		"question": "* 48.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Consegue correlacionar na sua estratégia de gestão de contas com cada interlocutor, sabendo quem e como abordar cada um dependendo do tema/produto.",
			"val": 0,
			"id": "295468808_1973309381"
		}, {
			"caption": "Viabiliza que todos tenham as mesmas oportunidades de aprendizado, seja por meio de incentivo ao autodesenvolvimento ou a troca entre pessoas com experiências profissionais diferentes, exemplo: job rotation.",
			"val": 0,
			"id": "295468808_1973309382"
		}]
	}, {
		"question": "* 49.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Possui informações que cruza possíveis riscos com o perfil do interlocutor que tem influência e poder de decisão.",
			"val": 0,
			"id": "295468809_1973309385"
		}, {
			"caption": "Possui foco na solução e não no problema, mantendo uma postura otimista e flexível.",
			"val": 0,
			"id": "295468809_1973309386"
		}]
	}, {
		"question": "* 50.* Escolha abaixo apenas um dos comportamentos que precisa ser desenvolvido ou melhorado.",
		"options": [{
			"caption": "Gerencio algumas exceções , garantindo que o cliente seja atendido, tendo sempre em mente o equilíbrio entre os processos internos e a flexibilização possível.",
			"val": 0,
			"id": "295468810_1973309389"
		}, {
			"caption": "Conhece os pontos fortes e fracos de cada um do time, mantendo conversas mais claras para o desenvolvimento individual.",
			"val": 0,
			"id": "295468810_1973309390"
		}]
	}, {
		"question": "51 Agora você vai escolher 10 temas que você identifica como necessários para o seu desenvolvimento. Ou seja, quais dos assuntos abaixo você considera importante para&nbsp;aprender mais.",
		"options": [{
			"val": "1973309109",
			"id": "295468754_1973309109",
			"caption": "\n                    Empreendedorismo\n                    \n                "
		}, {
			"val": "1973309110",
			"id": "295468754_1973309110",
			"caption": "\n                    Negociação\n                    \n                "
		}, {
			"val": "1973309111",
			"id": "295468754_1973309111",
			"caption": "\n                    Equilíbrio Emocional\n                    \n                "
		}, {
			"val": "1973309114",
			"id": "295468754_1973309114",
			"caption": "\n                    Resistência à Frustração\n                    \n                "
		}, {
			"val": "1973309115",
			"id": "295468754_1973309115",
			"caption": "\n                    Visão de Negócio\n                    \n                "
		}, {
			"val": "1973309116",
			"id": "295468754_1973309116",
			"caption": "\n                    Planejamento\n                    \n                "
		}, {
			"val": "1973309117",
			"id": "295468754_1973309117",
			"caption": "\n                    Síntese\n                    \n                "
		}, {
			"val": "1973309118",
			"id": "295468754_1973309118",
			"caption": "\n                    Administração de Conflitos\n                    \n                "
		}, {
			"val": "1973309119",
			"id": "295468754_1973309119",
			"caption": "\n                    Comunicação\n                    \n                "
		}, {
			"val": "1973309120",
			"id": "295468754_1973309120",
			"caption": "\n                    Foco em Resultados\n                    \n                "
		}, {
			"val": "1973309121",
			"id": "295468754_1973309121",
			"caption": "\n                    Diagnóstico\n                    \n                "
		}, {
			"val": "1973309122",
			"id": "295468754_1973309122",
			"caption": "\n                    Pensamento Estratégico\n                    \n                "
		}, {
			"val": "1973309123",
			"id": "295468754_1973309123",
			"caption": "\n                    Organização\n                    \n                "
		}, {
			"val": "1973309124",
			"id": "295468754_1973309124",
			"caption": "\n                    Análise Crítica\n                    \n                "
		}, {
			"val": "1973309316",
			"id": "295468754_1973309316",
			"caption": "\n                    Autoconfiança\n                    \n                "
		}, {
			"val": "1973309317",
			"id": "295468754_1973309317",
			"caption": "\n                    Capacidade de dar Feedback\n                    \n                "
		}, {
			"val": "1973309125",
			"id": "295468754_1973309125",
			"caption": "\n                    Criatividade e Inovação\n                    \n                "
		}, {
			"val": "1973309318",
			"id": "295468754_1973309318",
			"caption": "\n                    Liderança\n                    \n                "
		}, {
			"val": "1973309319",
			"id": "295468754_1973309319",
			"caption": "\n                    Capacidade de Gerir Mudanças\n                    \n                "
		}, {
			"val": "1973309126",
			"id": "295468754_1973309126",
			"caption": "\n                    Empatia\n                    \n                "
		}, {
			"val": "1973309320",
			"id": "295468754_1973309320",
			"caption": "\n                    Capacidade de Inspirar Confiança\n                    \n                "
		}, {
			"val": "1973309127",
			"id": "295468754_1973309127",
			"caption": "\n                    Cumprimento de Prazos\n                    \n                "
		}, {
			"val": "1973309128",
			"id": "295468754_1973309128",
			"caption": "\n                    Adaptabilidade\n                    \n                "
		}, {
			"val": "1973309129",
			"id": "295468754_1973309129",
			"caption": "\n                    Capacidade de receber Feedback\n                    \n                "
		}, {
			"val": "1973309130",
			"id": "295468754_1973309130",
			"caption": "\n                    Flexibilidade\n                    \n                "
		}, {
			"val": "1973309131",
			"id": "295468754_1973309131",
			"caption": "\n                    Iniciativa\n                    \n                "
		}, {
			"val": "1973309132",
			"id": "295468754_1973309132",
			"caption": "\n                    Tomada de decisão\n                    \n                "
		}, {
			"val": "1973309133",
			"id": "295468754_1973309133",
			"caption": "\n                    Produtividade\n                    \n                "
		}, {
			"val": "1973309134",
			"id": "295468754_1973309134",
			"caption": "\n                    Relacionamento Interpessoal\n                    \n                "
		}, {
			"val": "1973309135",
			"id": "295468754_1973309135",
			"caption": "\n                    Trabalho em Equipe\n                    \n                "
		}, {
			"val": "1973309136",
			"id": "295468754_1973309136",
			"caption": "\n                    Disciplina\n                    \n                "
		}, {
			"val": "1973309137",
			"id": "295468754_1973309137",
			"caption": "\n                    Resolução de Problemas\n                    \n                "
		}, {
			"val": "1973309138",
			"id": "295468754_1973309138",
			"caption": "\n                    Foco no Cliente\n                    \n                "
		}]
	},
	{
		"question": "52. Para finalizar, você vai escolher 10 capacidades que você identifica como seus pontos fortes em termos de competências.",
		"options": [{
			"val": "1973309146",
			"id": "295468755_1973309146",
			"caption": "\n                    Capacidade de identificar oportunidades e fazer negócios.\n                    \n                "
		}, {
			"val": "1973309147",
			"id": "295468755_1973309147",
			"caption": "\n                    Capacidade de convergir interesses, beneficiando ambas as partes.\n                    \n                "
		}, {
			"val": "1973309148",
			"id": "295468755_1973309148",
			"caption": "\n                    Capacidade de saber lidar com as emoções.\n                    \n                "
		}, {
			"val": "1973309151",
			"id": "295468755_1973309151",
			"caption": "\n                    Capacidade de suportar pressão.\n                    \n                "
		}, {
			"val": "1973309152",
			"id": "295468755_1973309152",
			"caption": "\n                    Capacidade de seguir ordens e regras.\n                    \n                "
		}, {
			"val": "1973309153",
			"id": "295468755_1973309153",
			"caption": "\n                    Capacidade de prever, dividir e priorizar tarefas.\n                    \n                "
		}, {
			"val": "1973309154",
			"id": "295468755_1973309154",
			"caption": "\n                    Capacidade de reunir dados e chegar a uma visão objetiva.\n                    \n                "
		}, {
			"val": "1973309155",
			"id": "295468755_1973309155",
			"caption": "\n                    Capacidade de atuar na solução de conflitos.\n                    \n                "
		}, {
			"val": "1973309179",
			"id": "295468755_1973309179",
			"caption": "\n                    Capacidade de expressar com clareza seus pensamentos e opiniões.\n                    \n                "
		}, {
			"val": "1973309156",
			"id": "295468755_1973309156",
			"caption": "\n                    Capacidade de desempenhar atividades financeiras.\n                    \n                "
		}, {
			"val": "1973309157",
			"id": "295468755_1973309157",
			"caption": "\n                    Capacidade de detalhar precisamente uma situação/cenário.\n                    \n                "
		}, {
			"val": "1973309158",
			"id": "295468755_1973309158",
			"caption": "\n                    Capacidade de projetar ideias, visando o alcance dos objetivos propostos.\n                    \n                "
		}, {
			"val": "1973309159",
			"id": "295468755_1973309159",
			"caption": "\n                    Capacidade de organizar o tempo e o espaço, prezando a qualidade.\n                    \n                "
		}, {
			"val": "1973309160",
			"id": "295468755_1973309160",
			"caption": "\n                    Capacidade de emitir opiniões baseadas na análise prévia.\n                    \n                "
		}, {
			"val": "1973309321",
			"id": "295468755_1973309321",
			"caption": "\n                    Capacidade de valorizar-se e confiar em seus atos e julgamentos.\n                    \n                "
		}, {
			"val": "1973309322",
			"id": "295468755_1973309322",
			"caption": "\n                    Capacidade de dar informações sobre o comportamento alheio.\n                    \n                "
		}, {
			"val": "1973309161",
			"id": "295468755_1973309161",
			"caption": "\n                    Capacidade de criar ou melhorar um produto ou serviço.\n                    \n                "
		}, {
			"val": "1973309162",
			"id": "295468755_1973309162",
			"caption": "\n                    Capacidade de aplicar conhecimentos nas diversas situações.\n                    \n                "
		}, {
			"val": "1973309323",
			"id": "295468755_1973309323",
			"caption": "\n                    Capacidade de conduzir e motivar pessoas.\n                    \n                "
		}, {
			"val": "1973309324",
			"id": "295468755_1973309324",
			"caption": "\n                    Capacidade de incentivar outrem à mudança de comportamento.\n                    \n                "
		}, {
			"val": "1973309178",
			"id": "295468755_1973309178",
			"caption": "\n                    Capacidade de perceber e compreender os sentimentos e opiniões alheias.\n                    \n                "
		}, {
			"val": "1973309325",
			"id": "295468755_1973309325",
			"caption": "\n                    Capacidade de demonstrar certeza aos outros, fortalecendo a crença.\n                    \n                "
		}, {
			"val": "1973309163",
			"id": "295468755_1973309163",
			"caption": "\n                    Capacidade de cumprir os prazos estipulados para realizar as tarefas.\n                    \n                "
		}, {
			"val": "1973309164",
			"id": "295468755_1973309164",
			"caption": "\n                    Capacidade de trabalhar em situações complexas, ambíguas e variáveis.\n                    \n                "
		}, {
			"val": "1973309165",
			"id": "295468755_1973309165",
			"caption": "\n                    Capacidade de receber informações sobre seu próprio comportamento.\n                    \n                "
		}, {
			"val": "1973309166",
			"id": "295468755_1973309166",
			"caption": "\n                    Capacidade de abertura para rever suas posições e receber novas ideias.\n                    \n                "
		}, {
			"val": "1973309167",
			"id": "295468755_1973309167",
			"caption": "\n                    Capacidade de agir com prontidão e independência, criando soluções.\n                    \n                "
		}, {
			"val": "1973309326",
			"id": "295468755_1973309326",
			"caption": "\n                    Capacidade de resolver situações, demonstrando coragem e firmeza.\n                    \n                "
		}, {
			"val": "1973309168",
			"id": "295468755_1973309168",
			"caption": "\n                    Capacidade de utilizar o tempo, otimizando as atividades e a produção.\n                    \n                "
		}, {
			"val": "1973309169",
			"id": "295468755_1973309169",
			"caption": "\n                    Capacidade de se relacionar com outras pessoas, criando parcerias.\n                    \n                "
		}, {
			"val": "1973309170",
			"id": "295468755_1973309170",
			"caption": "\n                    Capacidade de interagir, cooperar e se solidarizar com as pessoas.\n                    \n                "
		}, {
			"val": "1973309171",
			"id": "295468755_1973309171",
			"caption": "\n                    Capacidade de cumprir as determinações e diretrizes corporativas.\n                    \n                "
		}, {
			"val": "1973309172",
			"id": "295468755_1973309172",
			"caption": "\n                    Capacidade de captar objetivamente os dados mais relevantes.\n                    \n                "
		}, {
			"val": "1973309173",
			"id": "295468755_1973309173",
			"caption": "\n                    Capacidade de executar tarefas que eliminem a causa do problema.\n                    \n                "
		}, {
			"val": "1973309174",
			"id": "295468755_1973309174",
			"caption": "\n                    Capacidade de atender e satisfazer as necessidades dos clientes.\n                    \n                "
		}]
	}
]';
 $quizParsed = json_decode($quiz);


if( isset($_GET['migrate']) =='5' ){


    foreach ($quizParsed as $key => $question) {
        $id = wp_insert_post(array(
          'post_title'=> $question->question, 
          'post_type'=>'quiz', 
          'post_content'=>'',
          'post_status' => 'publish',
         'orderby'=> 'title', 
         'order' => 'ASC'

        ));

        $optionsQ = [];
        foreach($question->options as $answer ){
            $answer->id = 'ANSWER_'.$id;
            $answer->val = 'OPTION_'.$id*rand(00,99);
        }


        add_post_meta($id, 'options', base64_encode(serialize(json_encode($question->options))));
        add_post_meta($id, 'index', $i);
        $i++;

    }
}
// $quizParsed = json_decode($quiz);

// foreach ($quizParsed as $key => $question) {
//     $id = wp_insert_post(array(
//       'post_title'=> $question->question, 
//       'post_type'=>'quiz', 
//       'post_content'=>''
//     ));


//     foreach($question->options as $answers ){
//         $answer->id = 'ANSWER_'.$id;
//     }


//     add_post_meta($id, 'options', serialize(json_encode($question->options)));

// }

add_action( 'init', 'creat_quiz_cpt' );
function creat_quiz_cpt() {

    register_post_type( 'quiz',
        array(
            'labels' => array(
                'name' => __( 'Quiz' ),
                'singular_name' => __( 'Quiz' )
            ),
        'public' => true,
        'has_archive' => true,
        'supports' => array('title'),
        )
    );
}

add_action( 'wp_ajax_login_wpw', 'login_main' );
add_action( 'wp_ajax_nopriv_login_wpw', 'login_main' );
function login_main() {
	session_start();

	if( !isset($_POST['user']) || !isset($_POST['pass']) ){
        http_response_code(500);

		echo json_encode(array('error'=>001, 'message'=> 'Invalid auth', 'code' => 'InvalidAuth'));
		exit;
	}

   	$creds = array(
        'user_login'    => esc_html( $_POST['user'] ),
        'user_password' => esc_html( $_POST['pass'] ),
        'remember'      => $_POST['remember']
    );
 
    $user = wp_signon( $creds, false );
 
    if ( is_wp_error( $user ) ) {
        http_response_code(500);

    	echo json_encode(array('error'=> 001, 'message'=> 'Dados inválidos', 'code' => 'InvalidAuth'));
      	exit;
    }else{
        $users = get_users( array( 'fields' => array( 'ID' ) ) );
        foreach($users as $user_id){
            $usersX[] = array( 'dob'=>get_user_meta ($user_id->ID,'dob', true), 'meta' => get_userdata ($user_id->ID), 'id'=> $user_id->ID );
        }        

        $current_user = wp_get_current_user();
        if (user_can( $current_user, 'administrator' )) {
          // user is an admin
                echo json_encode(array('error'=> false, 'admin' => true, 'user'=> $user, 'users' => $usersX,  'message'=> 'Sucess', 'code' => 'ValidAuth'));
                exit;    
        }

    	$_SESSION['merck'] = $user;
    	echo json_encode(array('error'=> false, 'admin' => false, 'message'=> 'Sucess', 'code' => 'ValidAuth'));
    	exit;


    }
}



function my_enqueue() {


    wp_enqueue_script( 'merck-ng', get_template_directory_uri() . '/assets/js/App.js', array('jquery') );

    wp_localize_script( 'merck-ng', 'Merck',
            array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}
add_action( 'wp_enqueue_scripts', 'my_enqueue' );





add_action('wp_ajax_register_user', 'register_user' ); 
add_action('wp_ajax_nopriv_register_user', 'register_user' ); 


add_action('wp_ajax_get_quiz', 'get_quiz' ); 
add_action('wp_ajax_nopriv_get_quiz', 'get_quiz' ); 


add_action('wp_ajax_login_dob', 'login_dob' ); 
add_action('wp_ajax_nopriv_login_dob', 'login_dob' ); 


add_action('wp_ajax_save_answers', 'save_answers' ); 
add_action('wp_ajax_nopriv_save_answers', 'save_answers' ); 


add_action('wp_ajax_fetch_user', 'fetch_user' ); 
add_action('wp_ajax_nopriv_fetch_user', 'fetch_user' ); 


function save_answers() {



	if( !isset($_POST['user']['ID']) ){

	    http_response_code(500);

		echo json_encode(array('error'=>001, 'message'=> 'Invalid auth', 'code' => 'InvalidAuth'));
		exit;
	}

	$data = $_POST;
	$args = array();
	$data['data'] = array_reverse($data['data']);
	foreach ($data['data'] as $key => $value) {
		$optionid = $value['ID'];
		$user_id = $data['user']['ID'];


		$args[]  = array(
			'option_id' => $optionid, 
			'user_id' => $user_id,
			'answer' => base64_encode(serialize(json_encode($value['_']))),
			'answer-raw' => json_encode($value['_'])
		);


		//update_user_meta( $user_id, 'user_id', $user_id);
		//update_user_meta( $user_id, 'answer', base64_encode(serialize(json_encode($value['_']))));
		//update_user_meta( $user_id, 'answer-raw', json_encode($value['_']);

	}
	update_user_meta( $user_id, 'result', $args);
	update_user_meta( $user_id, 'allowed', 'No');


    echo json_encode(array('error'=> false, 'message'=> 'Sucess', 'code' => 'ValidRecord'));
    exit();
	//array('id' => , 'answer'=> )
	
}


function get_quiz() {
	if ( get_query_var('paged') ) {
	    $paged = get_query_var('paged');
	} elseif ( get_query_var('page') ) {
	    $paged = get_query_var('page');
	} else {
	    $paged = 1;
	}	
    $query = new WP_Query(array(
        'post_type'=> 'quiz', 
        'posts_per_page'=>-1,
        'meta_key' => 'index',
        'orderby' => 'meta_value_num',
        'order' => 'ASC' ,
  
        ));
    $res =[];
    if( $query->have_posts() ):
        while( $query->have_posts() ):
            $query->the_post();
            $res[] = array(
                'title' => get_the_title(),
                'options' => json_decode(unserialize(base64_decode(get_post_meta( get_the_ID(), 'options', true)))),
                'index' => get_post_meta( get_the_ID(), 'index', true),
                'ID' => get_the_ID()
            );
        endwhile;
    endif;
        
    die(json_encode($res));
        // add_post_meta($id, 'options', serialize(json_encode($question->options)));

}



function fetch_user() {

	$user = get_user_by('id', $_POST['id']);

    $res = [];

    $meta = get_user_meta ($user->ID);

        
    die(json_encode(array( 'user'=> $user->data, 'meta'=>$meta)));
        // add_post_meta($id, 'options', serialize(json_encode($question->options)));

}

function register_user(){

    $pass = esc_html( $_POST['pwd'] );



    if( !isset( $_POST['pwd'] ) ):
        $pass = wp_generate_password( 12, false );
    else:endif;

    if( !isset($_POST['dob']) ||  $_POST['dob'] =='' ):
        http_response_code(500);
        exit( 
            json_encode(
                array(
                    'error' => true,
                    'message' => 'Data de nascimento obrigatório'
                )
            )
        );
    else:

    endif;


    if( !isset($_POST['pwd']) ||  $_POST['pwd'] =='' ):
        http_response_code(500);
        exit( 
            json_encode(
                array(
                    'error' => true,
                    'message' => 'Senha inválida'
                )
            )
        );
    else:

    endif;

    if( !isset($_POST['name']) ||  $_POST['name'] ==''):
        http_response_code(500);
        exit( 
            json_encode(
                array(
                    'error' => true,
                    'message' => 'Insira um nome de usuário'
                )
            )
        );
    else:

    endif;

    if( !isset($_POST['email']) || !is_email($_POST['email'])):
        http_response_code(500);

        exit( 
            json_encode(
                array(
                    'error' => true,
                    'message' => 'Insira um email válido'
                )
            )
        );
    else:

    endif;





    $user_login = esc_html($_POST['name']);
    $user_email = esc_html($_POST['email']);

	$user = get_user_by('email', $user_email);

	if( $user ){
        http_response_code(400);

        exit( 
            json_encode(
                array(
                    'error' => true,
                    'message' => 'Email já utilizado'
                )
            )
        );		
	}


    $errors = wp_create_user( $user_login, $pass, $user_email );

    if ( !is_wp_error($errors) ) {
        $from = get_option('admin_email');
        $headers = 'From: '.$from . "\r\n";
        $subject = "Registro efeutado dados de acesso - Merck";
        $msg = "Bem vindo ao Merck! \nInfo:\nUsuário: $username\nSenha: $random_password";
        wp_mail( $user_email, $subject, $msg, $headers );
        
        update_user_meta( $errors, 'dob', $_POST['dob'] );
        update_user_meta( $errors, 'allowed', 'Yes' );

        http_response_code(200);

        echo json_encode(array('error'=> false, 'message'=> 'Sucess', 'code' => 'ValidRecord'));
        exit();
    }   
    else{
        http_response_code(400);



        exit( 
            json_encode(
                array(
                    'error' => true,
                    'message' => $errors->errors['existing_user_login'][0]
                )
            )
        );	

    }
}

function login_dob(){
	session_start();
	if( !isset($_POST['email']) || !isset($_POST['dob']) ){
        http_response_code(500);

		echo json_encode(array('error'=>001, 'message'=> 'Invalid auth', 'code' => 'InvalidAuth'));
		exit;
	}

	$user = get_user_by( 'email', $_POST['email'] );


	 $dob = get_user_meta( $user->ID, 'dob', true );
	 //$allowed = get_user_meta( $user->ID, 'allowed', true );

	 $allowed = true;
	if( !isset($allowed) || $allowed == '' ){
        http_response_code(500);

		echo json_encode(array('error'=>001, 'message'=> 'Você não está autorizado a preencher o formulário.', 'code' => 'InvalidAuth'));
		exit;
	}

	if( $_POST['dob'] == $dob ){

        $users = get_users( array( 'fields' => array( 'ID' ) ) );
        foreach($users as $user_id){
            $usersX[] = array( 'meta' => get_userdata ($user_id->ID), 'id'=> $user_id->ID );
        }        

        $current_user = wp_get_current_user();
        if (user_can( $current_user, 'administrator' )) {
          // user is an admin
                echo json_encode(array('error'=> false, 'admin' => true, 'user'=> $user, 'users' => $usersX,  'message'=> 'Sucess', 'code' => 'ValidAuth'));
                exit;    
        }

    	$_SESSION['merck'] = $user;

	    wp_set_current_user ( $user->ID );
	    wp_set_auth_cookie  ( $user->ID );    	
    	echo json_encode(array(
    		'error'=> false, 
    		'allowed' => get_user_meta($current_user->ID, 'allowed', true), 
    		'admin' => false, 
    		'message'=> 'Sucess', 
    		'code' => 'ValidAuth'
    	));

    	exit;


	}else{
        http_response_code(500);

		echo json_encode(array('error'=> 001, 'message'=> 'Dados inválidos', 'code' => 'InvalidAuth'));
		exit;
	}
	//exit;	




  
}


?>
